const express = require('express');
const path = require('path');

const dirRoot = require('../util/path');
// const adminData = require('./adminRoute');

const router = express.Router();

//add controller 
const shopController = require('../controllers/shop.controller');

router.get('/', shopController.getProduct);
router.get('/shop/products', shopController.getProductList);
router.get('/shop/product-details/:id', shopController.getProductDetails);

router.get('/shop/cart', shopController.getCart);
// // router.get('/shop/checkout', shopController.getCheckOut);
router.post('/cart', shopController.postCart);
router.post('/cart/delete', shopController.deleteCart);

router.get('/shop/order', shopController.getOrder);
router.post('/shop/order', shopController.postOrder);


exports.routes = router; 