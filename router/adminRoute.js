const express = require('express');
const path = require('path');

// create a util/healper file to resolve root path(use to sendFile)
const dirRoot = require('../util/path');
const router = express.Router();

// add controller 
const adminController = require('../controllers/admin.controller');

 
//router call for the app 
router.get('/add-product', adminController.addProduct);
router.get('/products', adminController.getProduct); 
router.post('/edit-product', adminController.postEditProduct);
router.post('/add-product', adminController.postAddProduct);
router.get('/add-product/:productId', adminController.editProduct);
router.post('/product/delete/:productId', adminController.postDeleteProduct);

// expoers functionality/variable that we need after
exports.routes = router; 