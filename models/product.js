const mongoDb = require('mongodb');
// const getDb = require('../util/database').getDb;
const  mongoose = require('mongoose');
const Schema = mongoose.Schema;


const Product = new Schema({
    title: String,
    imageUrl: String,
    price: Number,
    description: String,
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
}); 

 
//class Porduct {
    // constructor(title, price, imageUrl, description, _id, userId) {
    //     this.title = title;
    //     this.imageUrl = imageUrl;
    //     this.price = price; 
    //     this.description = description;
    //     this._id = _id ? new mongoDb.ObjectId(_id) : null;
    //     this.userId = userId;
    // }

    // save(){
    //     const db = getDb();
    //     console.log(this._id);

    //     if(this._id){
    //         console.log('update');             
    //         return db.collection('products').updateOne({'_id': this._id}, {$set: this}).then(result=>{
    //             // console.log(result);
    //         }).catch(err=>{
    //             console.log(err);
    //         });
    //     }else{
    //         console.log('crate');            
    //         return db.collection('products').insertOne(this).then(result=>{
    //             // console.log(result);
    //         }).catch(err=>{
    //             console.log(err); 
    //         });
    //     }
        
      
    // }

    // static fatchAll(){
    //     const db = getDb();
    //    return db.collection('products').find().toArray().then(products=>{
    //         // console.log(products);   
    //         return products;
    //     }).catch(err=>{
    //         console.log(err);
            
    //     })
    // }

    // static findById(prodId){
    //     const db = getDb();
    //    return db.collection('products').find({_id: mongoDb.ObjectId(prodId)}).next().then(product=>{
    //         return product;
    //     }).catch(err=>{
    //         console.log(err);
    //     })
    // }

    // static delete(prodId){
    //     const db = getDb();
    //     return db.collection('products').deleteOne({_id: new mongoDb.ObjectId(prodId)}).then(result=>{
    //         console.log(result);
    //     }).catch(err=>{
    //         console.log(err);
    //     })
    // }


//}


 
module.exports = mongoose.model('Product', Product);
