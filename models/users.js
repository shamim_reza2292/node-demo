
const Products = require('./product');

const mongoose = require('mongoose');
const schema = mongoose.Schema;

const Users = new schema({
    userName: String, 
    email: String, 
    cart: {
        item: [{
            productId: {type: schema.Types.ObjectId, ref: 'Product'},
            quantity: Number
        }]
    }
})

Users.methods.addtoCart = function(productId, cart){
            let cartItemIndex;
            
            console.log(cart)
            if(cart){
                cartItemIndex = cart.item.findIndex(cartPorduct=>{
                    return  String(cartPorduct.productId) == String(productId); // use String() for object convert string
                });
            }
            let quantity = 1;  
            let updateCartItem = [];  
             
              updateCartItem = [ ...cart.item]
            
            console.log(cartItemIndex)

            if(cartItemIndex < 0){
                console.log('item not exist')
                updateCartItem.push({
                    productId: productId, 
                    quantity: quantity
                });
            }else{
                console.log('item alredy exist')
                quantity = cart.item[cartItemIndex].quantity + 1;
                updateCartItem[cartItemIndex].quantity = quantity;
            }
    
            let updateCart = {
                item: updateCartItem
            } 
           this.cart = updateCart; 
            return this.save();
}

 
Users.methods.postDeleteCart = function(cartItemId, cart) {
            // console.log('post delete cart');

            this.cart = cart;

            console.log(cartItemId);
            let updateCartItem = this.cart.item.filter(prod=>{
                return String(prod._id) !== String(cartItemId);
            });
            console.log(updateCartItem);


            this.cart = { item: updateCartItem};
            console.log(this.cart);
            return this.save();


            // const db = getDb();
            // return db.collection('users').updateOne({_id: this.id}, {$set: {cart: {item: updateCartItem}}}).then(data=>{
            //     // console.log(data);
            // }).catch(err=>{
            //     console.log(err);
            // })
        }

// class Users {

//     constructor (userName, email, cart, id){
//         this.userName = userName;
//         this.email = email;
//         this.cart = cart;
//         this.id = new mongodb.ObjectId(id);
//         // console.log('constructor....');
//         console.log(this.id);
//     }
 
//     save(){
//         const db = getDb();
//         db.collection('users').insertOne(this).then(result=>{
//             console.log('crate user');
//         }).catch(err=>{
//             console.log(err);
//         })
//     }

//     // cart add in user collection
//     addtoCart(productId){
//          let cartItemIndex;
//         if(this.cart){
//             cartItemIndex = this.cart.item.findIndex(cartPorduct=>{
//                 return cartPorduct.productId == productId;
//             });
//         }
            
//         let quantity = 1;  
//         let updateCartItem = [];
//         if(this.cart){
//           updateCartItem = [ ...this.cart.item]
//         }
//         if(cartItemIndex !== undefined && cartItemIndex !== -1){
//             quantity = this.cart.item[cartItemIndex].quantity + 1;
//             updateCartItem[cartItemIndex].quantity = quantity;
//         }else{
//             updateCartItem.push({
//                 productId: productId, 
//                 quantity: quantity
//             });
//         }

//         let updateCart = {
//             item: updateCartItem
//         }
//         const db = getDb(); 
//         return db.collection('users').updateOne({'_id': this.id}, {$set: {'cart': updateCart} }).then(data=>{
//             // console.log(data); 
//         }).catch(err=>{
//             console.log(err)
//         })
//     }

//     // get cart item 

//     getCart(){
//         if(this.cart){
//             let cartProductID = this.cart.item.map(prod=>{
//                 return new mongodb.ObjectId (prod.productId);
//             })
//             const db = getDb();
//            return db.collection('products').find({_id: {$in: cartProductID}}).toArray().then(products=>{
//                 return products.map(p=>{
//                     return {...p, quantity: this.cart.item.find(product=>{
//                         return product.productId == p._id
//                     }).quantity}
//                 })
//             });
//         }
//     }

//     // delete cart 
//     postDeleteCart(productId){
//         console.log('post delete cart');
//         console.log(productId);
//         let updateCartItem = this.cart.item.filter(prod=>{
//             return prod.productId !== productId;
//         });
//         const db = getDb();
//         return db.collection('users').updateOne({_id: this.id}, {$set: {cart: {item: updateCartItem}}}).then(data=>{
//             // console.log(data);
//         }).catch(err=>{
//             console.log(err);
//         })
//     }

//     // order post
//     getOrder(){
//         const db = getDb();
//         return db.collection('orders').find({'user.id': this.id}).toArray().then(orderData=>{
//             return orderData;
//         }).catch(err=>{
//             console.log(err);
//         })
//     } 

//     postOrder(){
//        return this.getCart().then(products =>{
//             // console.log(products);
//             let order = {
//                 item: products,
//                 user: {
//                     id: this.id,
//                     username: this.userName,
//                     email: this.email
//                 }
//             }
//         const db = getDb();
//         return db.collection('orders').insertOne(order).then(result=>{
//             this.cart = { item: []};
//             db.collection('users').updateOne({_id: this.id}, {$set: {cart: { item: []}} }).then(userData=>{
//                 // console.log(userData);
//             })
//         })
//         }) 
//     }
 
//     static findById(userId){
//         const db  = getDb();
//       return db.collection('users').find({_id: new mongodb.ObjectId(userId)}).next().then(userData=>{
//             return userData;
//         }).catch(err=>{
//             console.log(err);
//         }) 
//     }

// }

module.exports = mongoose.model('User', Users);
