const fs = require('fs');
const path = require('path');


const p = path.join(
    path.dirname(process.mainModule.filename),
    'data',
    'cart.json'
);


module.exports = class Cart {

    static addProduct(id, productName, porductPrice) {
        fs.readFile(p, (err, fileCotent) => {
            let cart = { product: [], totalPrice: 0 };
            if (!err) {
                cart = JSON.parse(fileCotent);
            }
            let existingProductIndex = cart.product.findIndex(prod => prod.id == id);
            let existingProduct = cart.product[existingProductIndex];
            let updateProduct;

            if (existingProduct) {
                updateProduct = {...existingProduct };
                updateProduct.qty = updateProduct.qty + 1;
                cart.product = [...cart.product];
                cart.product[existingProductIndex] = updateProduct
            } else {
                updateProduct = { id: id, name: productName, qty: 1 };
                cart.product = [...cart.product, updateProduct]
            }
            cart.totalPrice = cart.totalPrice + +porductPrice;
            fs.writeFile(p, JSON.stringify(cart), (err) => {
                if (err) {}
            });
        })
    }

    static fatchAll(cb) {
        fs.readFile(p, (err, fileCotent) => {
            if (err) {
                cb([]);
            } else {
                cb(JSON.parse(fileCotent))
            }
        })
    }

    static deleteCart(id, price) {
        fs.readFile(p, (err, fileCotent) => {
            if (!err) {
                let cart = JSON.parse(fileCotent);
                let withOutDeletedProduct = cart.product.find(prod => prod.id != id);
                let deletedProduct = cart.product.find(prod => prod.id == id);

                let deletedProductQuantity = deletedProduct.qty;

                cart.product = [];
                if (withOutDeletedProduct != null) {
                    cart.product.push(withOutDeletedProduct);
                }
                cart.totalPrice = cart.totalPrice - (price * deletedProductQuantity);
                fs.writeFile(p, JSON.stringify(cart), (err) => {
                    if (err) {
                        console.log(err);
                    }
                });
            }
        })
    }


}