const Product = require('../models/product');

const mongoose = require('mongoose');


exports.getProduct = (req, res, next) => { 
    Product.find().then((products) => {
        res.render('admin/products', {
            pageTitle: 'admin products', 
            items: products,
            path: '/admin/products'
        }); 
    }).catch(err => {
        console.log(err);
    }); 

}

exports.addProduct = (req, res, next) => {
    res.render('admin/add-product', {
        pageTitle: 'add product',
        path: '/admin/add-product',
        edited: false
    });
}


exports.postAddProduct = (req, res, next) => {
    // productList.push({title: req.body.title});
    let title = req.body.title; 
    let price = req.body.price;
    let imageUrl = req.body.imageUrl;
    let description = req.body.description;
    let userId = req.users._id;
    const product = new Product({title: title, price: price, imageUrl: imageUrl, description: description, userId: userId});
    product.save().then(() => {
        res.redirect('/admin/products'); 
    }).catch(err => {
        console.log(err);
    });
}

exports.editProduct = (req, res, next) => {
    let edited = req.query.edited;
    let productId = req.params.productId;
    console.log(productId);
    Product.findById(productId).then((prod) => {
        res.render('admin/add-product', { 
            pageTitle: 'Edit product', 
            path: '/admin/add-product',
            edited: edited,
            product: prod
        })
    }).catch(err=>{
        console.log(err);
    });
}

exports.postEditProduct = (req, res, next) => {
    let productId = req.body.productId;
    let title = req.body.title;   
    let imageUrl = req.body.imageUrl; 
    let price = req.body.price;
    let description = req.body.description;
    // let userId = req.users._id;
    Product.findById(productId).then(product =>{
        product.title = title;
        product.price = price;
        product.imageUrl = imageUrl;
        product.description = description;
        product.save()
    }).then(result=>{
        res.redirect('/admin/products');
    });
    
}


exports.postDeleteProduct = (req, res, next) => {
    let productId = req.params.productId;
    Product.findByIdAndRemove(productId).then(data=>{
        // console.log(data);
        res.redirect('/admin/products');
    }).catch(err=>{
        console.log(err);
    });

}