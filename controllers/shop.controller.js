const Product = require('../models/product');
const Cart = require('../models/cart');

const Users = require('../models/users');
const Order = require('../models/order')

const db = require('../util/database');
// const users = require('../models/users');

// product get in shop page
exports.getProduct = (req, res, next) => {
    Product.find().then((products) => {
        console.log(products);
        res.render('shop/index', {
            pageTitle: 'My Shop',
            items: products, // porduct from database
            path: '/'
        });
    }).catch(err => {
        console.log(err);
    });

}

exports.getProductList = (req, res, next) => {
    Product.find().then((products) => {
        res.render('shop/product-list', {
            pageTitle: 'products',
            items: products, // porduct from database
            path: '/shop/products',
        });
    }).catch(err => {
        console.log(err);
    });
}

exports.getProductDetails = (req, res, next) => {
    let id = req.params.id;
    Product.findById(id).then((product) => {
        console.log(product);
        res.render('shop/product-details', {
            pageTitle: product.title,
            path: '/shop/products',
            product: product
        })
    }).catch(err => {
        console.log(err);
    })
}


exports.getCart = (req, res, next) => {
    // req.users.getCart(req.users.cart)
    // console.log(req.users.cart.item);
    req.users.populate('cart.item.productId')
    .execPopulate()
    .then(cartProducts=>{
        // console.log(cartProducts.cart.item);
        let products = cartProducts.cart.item
        res.render('shop/cart', {
            pageTitle: 'cart', 
            path: '/shop/cart',
            items: products
        })
        // console.log(cartProducts);
    })

    
}

exports.postCart = (req, res, next) => {
    let productId = req.body.id;
    let cartItem = req.users.cart;
    Product.findById(productId).then(product =>{
        req.users.addtoCart(product._id, cartItem).then(()=>{
            res.redirect('/shop/cart');
        });
    });
    
}

exports.deleteCart = (req, res, next) => { 
    let cartItemId = req.body.cartItemId;
    console.log('delete cart call..')
    req.users.postDeleteCart(cartItemId, req.users.cart).then(product=>{ 
        res.redirect('/shop/cart')
    })
    // Product.findById(productId, (product) => {
    //     Cart.deleteCart(productId, product.price);
    //     res.redirect('/shop/cart')

    // }); 
}


exports.getOrder = (req, res, next) => {
    // req.users.getOrder().then(orderProducts=>{
        
    // })
    Order 
   
    .populate({path: 'userId', select: 'email'})
    .exec()
    .then(orderProducts=>{
        console.log(orderProducts);
        res.render('shop/order', {
            pageTitle: 'order',
            path: '/shop/order', 
            products: orderProducts
        }) 
    }) 

}

exports.postOrder = (req, res, next) => {
    // req.users.postOrder().then(result=>{
    //     res.redirect('/shop/order');
    // })
    let userId = req.users._id;
    console.log(userId);
    const order = new Order({title: 'post one order', userId: userId});
    console.log(order);

    order.save().then(data=>{
        res.redirect('/shop/order');
    });




}


exports.getCheckOut = (req, res, next) => {
    res.render('shop/checkout', {
        pageTitle: 'checkout',
        path: '/shop/Checkout'
    })
}