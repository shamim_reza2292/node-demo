const http = require('http');
const path = require('path');

// mongoDb 
// const mongoDbClient = require('./util/database').mongoConnection;
const mongoose = require('mongoose');
const users = require('./models/users');



// express initialize/install 
const express = require('express');
const app = express();


// need to parse data after reuqest send
const bodyPerser = require('body-parser');
app.use(bodyPerser.urlencoded({ extended: false }));

// declear root folder of css by set it static
app.use(express.static(path.join(__dirname, 'public')));


//EJS template engine setting (npm install --save ejs)
app.set('view engine', 'ejs');
app.set('views', 'view')


// user info request 
app.use((req, res, next)=>{
  console.log('user finding');
  
  users.findById('5f58abeeef476cdd6d53549f').then(userData=>{
    // console.log(userData);
    // req.users = new Users(userData);
    req.users =  userData;
    // console.log(userData.cart);
    next() 
  }).catch(err=>{
    console.log(err);
  })
}); 
 

// error controller 
const errorController = require('./controllers/error');

//inisialize all roeute(all route called from child route) 
const adminRouter = require('./router/adminRoute');
const shopRouter = require('./router/shopRoute');
const Users = require('./models/users');
app.use('/admin', adminRouter.routes);
app.use(shopRouter.routes);
// 404 page send from here  
app.use(errorController.get404); 




//create a server with express.js
// const server = http.createServer(app);
 
// mongoDbClient((client)=>{
//   // console.log(client);
  
//   app.listen(5000);
// }) 

mongoose.connect('mongodb+srv://max:FZ4EhKBQkbW3Tnec@cluster0-xxcpo.mongodb.net/shop?retryWrites=true&w=majority', 
{useNewUrlParser: true, useUnifiedTopology: true})
.then(result =>{
  // console.log(result);
  console.log('conected');
  app.listen(5000);
})
